const { merge } = require('webpack-merge');
const main = require('./webpack.config.js');

module.exports = merge(main, {
  mode: 'production',
});
