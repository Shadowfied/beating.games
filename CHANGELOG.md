# [1.3.0](https://gitlab.com/Shadowfied/beating.games/compare/v1.2.0...v1.3.0) (2022-03-01)


### Features

* **footer:** remove gitlab link ([c840caa](https://gitlab.com/Shadowfied/beating.games/commit/c840caa50af1899a3f3236a3607b99e6fae1a23a))

# [1.2.0](https://gitlab.com/Shadowfied/beating.games/compare/v1.1.1...v1.2.0) (2020-12-19)


### Features

* **game:** ✨ add achievements link & perfect indicator ([0b27de1](https://gitlab.com/Shadowfied/beating.games/commit/0b27de1a0e9c531dfe0a0c5de9f69cab930e6d34)), closes [#12](https://gitlab.com/Shadowfied/beating.games/issues/12) [#11](https://gitlab.com/Shadowfied/beating.games/issues/11)

## [1.1.1](https://gitlab.com/Shadowfied/beating.games/compare/v1.1.0...v1.1.1) (2020-12-19)


### Bug Fixes

* 💚 run semrel before build to properly set version numbers ([f87fa9d](https://gitlab.com/Shadowfied/beating.games/commit/f87fa9dbaccdba834b059ef566485af19cec9848))

# [1.1.0](https://gitlab.com/Shadowfied/beating.games/compare/v1.0.1...v1.1.0) (2020-12-19)


### Features

* **game:** ✨ add 'added on' to game box on hover ([1f38b01](https://gitlab.com/Shadowfied/beating.games/commit/1f38b0100f40857c00bc34ec097141066b9b8bf0))
* **gamestatus:** ✨ added 'continual' game status ([1977e8c](https://gitlab.com/Shadowfied/beating.games/commit/1977e8ca1444537c951f3943940c165010445198)), closes [#13](https://gitlab.com/Shadowfied/beating.games/issues/13)

## [1.0.1](https://gitlab.com/Shadowfied/beating.games/compare/v1.0.0...v1.0.1) (2020-11-14)


### Bug Fixes

* **spoilertag:** ✨ respect spaces in spoiler garbage text ([2433006](https://gitlab.com/Shadowfied/beating.games/commit/24330062406307ec619d597be2e08df51947e522))

# 1.0.0 (2020-11-05)


### Features

* ✨ add lazy loading for game box images ([164e50f](https://gitlab.com/Shadowfied/beating.games/commit/164e50f5f158e0c20c660bc274748565721c35d4)), closes [#1](https://gitlab.com/Shadowfied/beating.games/issues/1)
