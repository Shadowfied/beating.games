import React, { useState, useEffect } from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
  Link,
  NavLink,
  Redirect,
} from 'react-router-dom';
import { Helmet } from 'react-helmet';

import Header from '../components/Header';
import NotFoundPage from '../components/Pages/NotFoundPage';
import HomePage from '../components/Pages/HomePage';
import UserPage from '../components/Pages/UserPage';
import SettingsPage from '../components/Pages/SettingsPage';
import 'normalize.css';
import { AuthContext } from '../context/auth';
import Footer from '../components/Footer';

const AppRouter = () => {
  const [authData, setAuthData] = useState();

  const _setAuthData = (data) => {
    if (data) {
      localStorage.setItem('auth', JSON.stringify(data));
      setAuthData(data);
    } else {
      localStorage.removeItem('auth');
      setAuthData('');
    }
  };

  useEffect(() => {
    if (localStorage.getItem('auth')) {
      setAuthData(JSON.parse(localStorage.getItem('auth')));
    }
  }, []);

  return (
    <AuthContext.Provider value={{ authData, setAuthData: _setAuthData }}>
      <Helmet>
        <meta property='og:locale' content='en_US' />
        <meta property='og:type' content='website' />
        <meta property='og:site_name' content='beating.games' />
      </Helmet>
      <BrowserRouter>
        <div>
          <div className='site-container container--fluid'>
            <Header />
            <Switch>
              <Route exact strict path='/' component={HomePage} />
              <Route exact strict path='/settings/' component={SettingsPage} />
              <Route
                exact
                strict
                path='/user/:username/'
                component={UserPage}
              />
              <Redirect to='/' />
            </Switch>
            <Footer />
          </div>
        </div>
      </BrowserRouter>
    </AuthContext.Provider>
  );
};

export default AppRouter;
