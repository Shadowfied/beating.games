import React from 'react';

const DropdownFilter = (props) => {
	const handleDropdownChange = (e) => {
		props.callback(e.target.value);
	};

	return (
		<React.Fragment>
			<label htmlFor={props.label}>{props.label}</label>
			<select id={props.label} onChange={handleDropdownChange}>
				{props.default && <option value={props.defaultValue ? props.defaultValue : ''}>{props.default}</option>}
				
				{props.options.constructor === Array ? (
					props.options && props.options.map((option, index) => (
						<option key={index} value={option}>{option}</option>
					))
				) : (
					props.options && Object.entries(props.options).map((option, index) => (
						<option key={index} value={option[1]}>{option[0]}</option>
					))
				)}
			</select>
		</React.Fragment>
	);
}

export default DropdownFilter;