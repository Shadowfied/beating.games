import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useAuth } from '../context/auth';
import useComponentVisible from '../hooks/useComponentVisible';

import Chevron from './UI/Chevron';
import Avatar from './UI/Avatar';

const Header = () => {
	const { authData, setAuthData } = useAuth();
	const [userActionsOpen, setUserActionsOpen] = useState(false);
	const { ref, isComponentVisible, setIsComponentVisible } = useComponentVisible(false);

	const toggleUserActions = () => {
		setUserActionsOpen(!userActionsOpen);
	};

	const actionLogout = () => {
		setAuthData(null);
	};

	return (
		<div className="container--fluid header">
			<div className="container header__inner">
				<div className="header__brand">
					<Link to="/">beating.games</Link>
				</div>
				{ authData && 
				<div className="header__user-info" ref={ref} onClick={() => setIsComponentVisible(!isComponentVisible)}>
					<Avatar background={authData.avatar} />
					<span className="current-user">{authData.user}</span>
					<i className="material-icons">keyboard_arrow_down</i>
					<div className={`user-actions`}>
					{isComponentVisible &&
						<React.Fragment>
							<Link to={`/user/${authData.user}/`}>Profile</Link>
							<Link to={`/settings/`}>Settings</Link>
							<span onClick={actionLogout}>Logout</span>
						</React.Fragment>
					}
					</div>
				</div>
				}
			</div>
		</div>
	);
};

export default Header;