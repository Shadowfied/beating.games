import React from 'react';

const pkg = require('../../package.json');

const Footer = () => {
  return (
    <div style={{ textAlign: 'center', padding: '1rem' }}>
      © {new Date().getUTCFullYear()} beating.games - {pkg.version} -{' '}
    </div>
  );
};

export default Footer;
