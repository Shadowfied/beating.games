import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import LoginForm from '../LoginForm';
import RegisterForm from '../RegisterForm';
import Avatar from '../UI/Avatar';
import ScrollToTop from '../UI/ScrollToTop';
import Feature from '../Feature';

import { useAuth } from '../../context/auth';

const HomePage = () => {
	const { authData } = useAuth();
	const [users, setUsers] = useState();
	const [showLoginForm, setShowLoginForm] = useState();
	const [showRegisterForm, setShowRegisterForm] = useState();

	const [apiIsDown, setApiIsDown] = useState(false);

	return (
		<div>
			<ScrollToTop />
			<Helmet>
				<title>beating.games - Keep track of your gaming activity online</title>
			</Helmet>

			<section className="front-page-hero">
				<div className="container">
					<div className="row middle-xs">
						<div className="col-xs-12 col-md-6">
							<h1>Track your gaming activity.<br />
							Track it in style.</h1>
							<hr />
							<p>beating.games is a beautiful way to organize video games that you are playing, have completed, or want to play.</p>
							{! authData ? (
								<React.Fragment>
									<div className="front-page-hero-actions">
											<button className="button button--primary" onClick={() => { setShowRegisterForm(!showRegisterForm) }}>Sign up</button>
											<button className="button button--secondary" onClick={() => { setShowLoginForm(!showLoginForm) }}>Log in</button>
									</div>
									{ showLoginForm && 
										<LoginForm />
									}
									{ showRegisterForm && 
										<RegisterForm />
									}
								</React.Fragment>
							) : (
								<React.Fragment>
									<Link className="button button--primary" to={`/user/${encodeURIComponent(authData.user)}/`}>View your profile</Link>
								</React.Fragment>
								)
							}
						</div>
						<div className="col-xs-12 col-md-5 col-md-offset-1 flex-center-vertical">
							<video autoPlay muted playsInline src="https://api.beating.games/video/demo.mp4" />
						</div>
					</div>
				</div>
			</section>

			<section className="front-page-features">
				<div className="container">
					<div className="row">
						<div className="col-xs-12" style={{textAlign: "center", marginBottom: "2rem"}}>
							<h2>Features of beating.games</h2>
						</div>
						<div className="col-xs-12 col-md-4">
							<Feature icon="games" title="Game tracking">
								Keep track of video games that you are playing, want to play and have beaten
							</Feature>
						</div>
						<div className="col-xs-12 col-md-4">
							<Feature icon="person" title="Profiles">
								Get your own customizable profile that you can share with friends
							</Feature>
						</div>
						<div className="col-xs-12 col-md-4">
							<Feature icon="filter_list" title="Filtering">
								Filter your list of games by status, system, year and more
							</Feature>
						</div>
					</div>
				</div>
			</section>

			<section className="front-page-information">
				<div className="container">
					<div className="row">
						<div className="col-xs-12" style={{ textAlign: "center", marginBottom: "4rem" }}>
							<h2>Are you aware of your gaming habits?</h2>
						</div>
					</div>
					<div className="row information-row middle-xs">
						<div className="col-xs-12 col-md-6">
							<h3>Overview</h3>
							<p>Are you aware of how much time you spend playing games? Are you actually finishing the games you buy? Have you ever considered the statistics of your gaming habits?</p>
							<p>beating.games makes it easy to manage which games you are currently playing, games you have finished, and games that you want to play in the future. By actually looking at the numbers you'll increase your motivation to finish games</p>
						</div>
						<div className="col-xs-12 col-md-6 first-xs last-md">
							<div className="information-icon">
								<i className="material-icons">access_time</i>
							</div>
						</div>
					</div>

					<div className="row information-row middle-xs">
						<div className="col-xs-12 col-md-6">
							<h3>Get your moneys worth</h3>
							<p>If you don't keep track of what games you're actually playing, you'll quickly find yourself in a position where you have too many games going at once. The next Steam sale will steal your wallet, while your backlog is ever-growing.</p>
							<p> By using beating.games you can increase the desire and motivation to finish the games you have already started.</p>
						</div>
						<div className="col-xs-12 col-md-6 first-xs">
							<div className="information-icon">
								<i className="material-icons">attach_money</i>
							</div>
						</div>
					</div>

					<div className="row information-row middle-xs">
						<div className="col-xs-12 col-md-6">
							<h3>Build your own database</h3>
							<p>We have no intention of locking you down! Your data is yours!</p>
							<p>beating.games lets you export your data as XML / CSV so you can easily take it wherever you want. If you'd rather manage it yourself, or if you find another service, your data is always yours. We want you to find the passion in gaming, whether that comes from our platform or someone elses is up to you!</p>
						</div>
						<div className="col-xs-12 col-md-6 first-xs last-md">
							<div className="information-icon">
								<i className="material-icons">storage</i>
							</div>
						</div>
					</div>
				</div>
			</section>

			{/*
			<div class="container">
				<div className="row sign-in-up">
					<div className="col-xs-12 col-md-4">
					{
						! authData ? (
							<React.Fragment>
								<h2>Register</h2>
								<RegisterForm onRegister={fetchUsers} />
							</React.Fragment>
						) : (
							<React.Fragment>
								<h2>Welcome {authData.user}!</h2>
								<Link className="button button--primary" style={{marginTop: "1rem"}} to={`/user/${encodeURIComponent(authData.user)}/`}>View profile</Link>
								<p>Click the button above to view your profile and get started, or visit a fellow gamebeater in the Users list.</p>
							</React.Fragment>
						) 
					}
					</div>
					<div className="col-xs-12 col-md-4">
						{ ! authData &&
							<React.Fragment>
								<h2>Login</h2>
								<LoginForm />
							</React.Fragment>
						}
					</div>

					<div className="col-xs-12 col-md-4">
						<h2>Users</h2>
						{ apiIsDown && 
							<div className="status-message status-message--error">Failed to fetch user list</div>
						}
						{ users && 
							<React.Fragment>
								<ul className="users-list">
									{users.map((user, index) => (
										<li key={index}>
											<Link to={`/user/${encodeURIComponent(user.username)}/`} className="user-listing">
												<Avatar className="user-listing__avatar" background={user.avatar} />
												<div className="user-listing__username"> 
													{user.username}
												</div>
												<div className="user-listing__stats">

												</div>
											</Link>
										</li>
									))}
								</ul>
							</React.Fragment>
						}
					</div>
				</div>
			</div>*/}



		</div>
	);
}

export default HomePage;