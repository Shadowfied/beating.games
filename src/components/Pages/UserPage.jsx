import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Helmet } from 'react-helmet';

import GamesList from '../GamesList';
import LoadingIndicator from '../UI/LoadingIndicator';

import UserBanner from '../UserBanner';

import { useAuth } from '../../context/auth';
import { useScrollPosition } from '../../hooks/useScrollPosition';

const UserPage = (props) => {
  const { authData } = useAuth();
  const [userData, setUserData] = useState();
  const [gameStats, setGameStats] = useState();
  const [isError, setIsError] = useState(false);
  const [userBannerIsSticky, setUserBannerIsSticky] = useState(false);
  const username = props.match.params.username;
  const pageTitle = `${username}'s profile at beating.games`;
  const pageDescription = `Check what games ${username} is playing, has played and wants to play on their beating.games profile.`;
  const canonical = `https://beating.games/user/${username}/`;

  useScrollPosition(({ prevPos, currPos }) => {
    if (currPos.y <= -300) {
      setUserBannerIsSticky(true);
    } else {
      setUserBannerIsSticky(false);
    }
  });

  const fetchUserData = () => {
    fetch(`https://api.beating.games/user/${encodeURIComponent(username)}`)
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw res;
        }
      })
      .then((data) => {
        if (data) {
          setUserData(data);
        }
      })
      .catch((err) => {
        console.log(err);
        setIsError(true);
      });
  };

  useEffect(() => {
    calculateGameStats();
  }, [userData]);

  const calculateGameStats = () => {
    if (userData) {
      let beatenGames = 0;
      let continualGames = 0;
      let inProgressGames = 0;
      let wantToPlayGames = 0;
      let droppedGames = 0;

      userData.games.filter((game) => {
        switch (game.status) {
          case 'Beaten':
            beatenGames++;
            break;
          case 'Continual':
            continualGames++;
            break;
          case 'In Progress':
            inProgressGames++;
            break;
          case 'Want To Play':
            wantToPlayGames++;
            break;
          case 'Dropped':
            droppedGames++;
        }
      });

      setGameStats({
        beatenGames: beatenGames,
        continualGames: continualGames,
        inProgressGames: inProgressGames,
        wantToPlayGames: wantToPlayGames,
        droppedGames: droppedGames,
      });
    }
  };

  useEffect(() => {
    fetchUserData();
  }, [props.match.params.username]);

  return (
    <React.Fragment>
      <Helmet>
        <title>{pageTitle}</title>
        <meta name='description' content={pageDescription} />
        <meta name='canonical' content={canonical} />
        <meta property='og:title' content={pageTitle} />
        <meta property='og:description' content={pageDescription} />
        <meta property='og:url' content={canonical} />
      </Helmet>
      <div className='container--fluid'>
        {isError && (
          <div className='container'>
            <Helmet>
              <title>Invalid user</title>
              <meta property='robots' content='noindex' />
            </Helmet>
            <h2 className='status-message status-message--error'>
              {' '}
              This user does not exist.
            </h2>
          </div>
        )}
        {!isError && !userData && <LoadingIndicator />}
        {userData && userData.user && gameStats && (
          <React.Fragment>
            <UserBanner
              sticky={userBannerIsSticky}
              user={username}
              gameStats={gameStats}
              background={userData.user.background}
              backgroundPosition={userData.user.backgroundPosition}
              backgroundFixed={userData.user.backgroundFixed}
              avatar={userData.user.avatar}
            />
            <div className='container'>
              <GamesList
                user={username}
                games={userData.games}
                onUpdate={fetchUserData}
              />
            </div>
          </React.Fragment>
        )}
      </div>
    </React.Fragment>
  );
};

export default UserPage;
