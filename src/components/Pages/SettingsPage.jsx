import React, { useState, useEffect } from 'react';

import LoadingIndicator from '../UI/LoadingIndicator';
import StatusMessage from '../UI/StatusMessage';
import Form from '../Form';

import { useAuth } from '../../context/auth';

const SettingsPage = (props) => {
	const { authData, setAuthData } = useAuth();

	const onSuccess = (res) => {
		setAuthData({...authData, ...res});
	};

	return (
		<div className="container page-container">
			<h1>Settings</h1>

			<Form 
				labels="true"
				fetch="https://api.beating.games/user/settings"
				fetchAuth="true"
				submit="https://api.beating.games/settings"
				submitAuth="true"
				submitSuccess="Settings updated!"
				submitSuccessCallback={onSuccess}
				submitError="Failed to save updates"
				fields={[
					{
						type: "URL",
						name: "avatar",
						niceName: "Avatar",
						description: "Choose a URL for your avatar"
					},
					{
						type: "URL",
						name: "background",
						niceName: "Background",
						description: "URL to a background image for your profile. Image uploading and hosting will be implemented but I recommend using imgur for now"
					},
					{
						type: "text",
						name: "backgroundPosition",
						niceName: "Background Position",
						description: `How you want your background to be positioned. This uses CSS positioning. For example, "center center" (without quotes) will center both axis', while "center top" will be centered horizontally but aligned top vertically. You can also use custom values like "center 4rem" to align it center on the X axis but 40 pixels down on the Y axis`
					},
					{
						type: "text",
						name: "backgroundFixed",
						niceName: "Fixed Background",
						description: `Write "true" in this box if you want your background image to be fixed when scrolling on your profile`
					},
					{
						type: "email",
						name: "email",
						niceName: "E-mail",
						description: `Fill in your email address. This will eventually be used for account recovery possibilities`
					}
				]}
			/>
		</div>
	);
};

export default SettingsPage;