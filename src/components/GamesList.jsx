import React, { useState, useEffect } from 'react';

import GameFilter from './GameFilter';
import Game from './Game';
import NewGameForm from './NewGameForm';

import { useAuth } from '../context/auth';

const GamesList = (props) => {
  const { authData } = useAuth();
  const [editingGame, setEditingGame] = useState();

  const [games, setGames] = useState(props.games);
  const [shownGames, setShownGames] = useState(props.games);
  const [gameTitleFilter, setGameTitleFilter] = useState('');
  const [gameSystemFilter, setGameSystemFilter] = useState('');
  const [gameStatusFilter, setGameStatusFilter] = useState('');
  const [gameOwnedFilter, setGameOwnedFilter] = useState(null);
  const [gameYearFilter, setGameYearFilter] = useState('');

  useEffect(() => {
    setGames(props.games);
    setShownGames(props.games);
  }, [props.games]);

  const collectField = (key) => {
    let fields = [];

    games.forEach((game) => {
      if (!fields.includes(game[key])) {
        fields.push(game[key]);
      }
    });

    return fields.sort();
  };

  const collectYears = () => {
    let years = [];

    games.forEach((game) => {
      let year = game.date.match(/[0-9]{4}/g);
      if (year) {
        if (!years.includes(year[0])) {
          years.push(year[0]);
        }
      }
    });

    return years;
  };

  const runFilters = () => {
    // Filter the list of games being shown
    setShownGames(
      [...games].filter((game) => {
        return (
          game.title.toLowerCase().includes(gameTitleFilter) &&
          game.system.indexOf(gameSystemFilter) !== -1 &&
          game.status.includes(gameStatusFilter) &&
          (gameOwnedFilter !== null ? game.owned == gameOwnedFilter : 'All') &&
          game.date.includes(gameYearFilter)
        );
      })
    );
  };

  useEffect(() => {
    runFilters();
  }, [
    games,
    gameOwnedFilter,
    gameTitleFilter,
    gameSystemFilter,
    gameStatusFilter,
    gameYearFilter,
  ]);

  const getFilterData = () => {
    return {
      systems: collectField('system'),
      statuses: collectField('status'),
      years: collectYears(),
      gameTitleFilterCallback: setGameTitleFilter,
      gameSystemFilterCallback: setGameSystemFilter,
      gameStatusFilterCallback: setGameStatusFilter,
      gameOwnedFilterCallback: setGameOwnedFilter,
      gameYearFilterCallback: setGameYearFilter,
    };
  };

  const editGame = (game) => {
    setEditingGame({ ...game });
  };

  return (
    <div className='container'>
      <div className='row'>
        <div className='col-xs-12 col-sm-6 col-lg-3'>
          <aside className='sidebar'>
            <div
              className='sticky'
              style={{
                maxHeight: '76vh',
                overflowY: 'auto',
                overflowX: 'hidden',
              }}
            >
              <NewGameForm
                update={props.onUpdate}
                user={props.user}
                editGame={editingGame}
              />
              <GameFilter filterData={getFilterData()} />
              Shown games: {shownGames.length} / {games.length}
            </div>
          </aside>
        </div>
        <div className='col-xs-12 col-sm-6 col-lg-8'>
          <div className='games-list'>
            {shownGames.map((game, index) => (
              <Game
                key={index}
                update={props.onUpdate}
                edit={editGame}
                user={props.user}
                game={game}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default GamesList;
