import React, { useState } from 'react';

import DropdownFilter from './DropdownFilter';

export default function GameFilter(props) {
  const handleTitleChange = (e) => {
    props.filterData.gameTitleFilterCallback(e.target.value);
  };

  return (
    <div className='row game-filters'>
      <div className='col-xs-12'>
        <input
          placeholder='Filter by name'
          onChange={handleTitleChange}
          type='text'
        />
        {/*
				<div className="col-xs-12 col-md-2">
					<DropdownFilter default="Descending" defaultValue="Descending" options={["Ascending"]} callback={props.filterData.orderDirectionCallback}/>
				</div>
				<div className="col-xs-12 col-md-2">
					<DropdownFilter default="Date" defaultValue="Date" options={["Title", "Playtime"]} callback={props.filterData.orderCallback}/>
				</div>
				*/}
        <DropdownFilter
          label='Filter by system'
          default='All'
          options={props.filterData.systems}
          callback={props.filterData.gameSystemFilterCallback}
        />
        <DropdownFilter
          label='Filter by year'
          default='All'
          options={props.filterData.years}
          callback={props.filterData.gameYearFilterCallback}
        />
        <DropdownFilter
          label='Filter by status'
          default='All'
          options={[
            'Beaten',
            'Continual',
            'In Progress',
            'Want To Play',
            'Dropped',
          ]}
          callback={props.filterData.gameStatusFilterCallback}
        />
        <DropdownFilter
          label='Filter by owned'
          default='All'
          options={{ Owned: true, 'Not owned': false }}
          callback={(e) => {
            const updateValue =
              e == 'true' || e == 'false' ? JSON.parse(e) : null;
            props.filterData.gameOwnedFilterCallback(updateValue);
          }}
        />
      </div>
    </div>
  );
}
