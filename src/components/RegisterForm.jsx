import React, { useState } from 'react';
import Form from './Form';

const RegisterForm = (props) => {
	const [hasRegistered, setHasRegistered] = useState(false);

	const onRegister = (data) => {
		setHasRegistered(true);
		props.onRegister();
	};

	return (
		<div className="login-form">
			<Form 
				submit="https://api.beating.games/register"
				submitSuccess="Your account has been created! Please login."
				submitSuccessCallback={onRegister}
				hideForm={hasRegistered}
				fields={[
					{
						type: "text",
						name: "username",
						niceName: "Username",
						required: true,
						autocomplete: false
					},
					{
						type: "password",
						name: "password",
						niceName: "Password",
						required: true,
						autocomplete: false
					},
					{
						type: "email",
						name: "email",
						niceName: "Email",
						required: true,
						autocomplete: false
					}
				]}
			/>
		</div>
	);
}

export default RegisterForm;