import React, { useState } from 'react';

const Modal = (props) => {
	const [isVisible, setIsVisible] = useState(false);

	return (
		<div className="modal">
			{props.children}
		</div>
	);
};

export default Modal;