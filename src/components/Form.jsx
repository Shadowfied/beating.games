import React, { useState, useEffect } from 'react';

import LoadingIndicator from './UI/LoadingIndicator';
import StatusMessage from './UI/StatusMessage';

import { useAuth } from '../context/auth';

/* props layout
ĺabels = set to true to show labels
fields = object of form fields to display and submit
fetch = API URL to fetch data from
fetchAuth = boolean to tell if the fetched URL requires auth
submit = API URL to submit data to
submitSuccess = string, message on successful submit
submitError = string, message to display on failed submit
submitSuccessCallback = function to run on successful submit
submitErrorCallback = function to run on failed submit
*/

const Form = (props) => {
  const initFields = () => {
    let fields = {};
    props.fields.map((field, index) => {
      fields[field.name] = '';
    });
    return fields;
  };

  const { authData } = useAuth();
  const [fields, setFields] = useState(initFields());
  const [hasFetched, setHasFetched] = useState(false);
  const [response, setResponse] = useState('');
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  if (props.fetch) {
    useEffect(() => {
      fetchFields();
    }, [authData]);
  }

  const fetchFields = () => {
    if (authData) {
      let headers = {};
      if (props.fetchAuth) {
        headers['Authorization'] = authData.token;
      }

      fetch(props.fetch, {
        headers: headers,
      })
        .then((response) => response.json())
        .then((data) => {
          setFields({ ...data });
          setHasFetched(true);
          if (props.fetchSuccessCallback) {
            return props.fetchSuccessCallback(data);
          }
        });
    }
  };

  const handleFormChange = (e) => {
    const { name, value } = e.target;
    setFields({ ...fields, [name]: value });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();

    if (props.submit) {
      let headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      };

      if (props.submitAuth) {
        headers['Authorization'] = authData.token;
      }

      fetch(props.submit, {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(fields),
      })
        .then((res) => {
          if (res.ok) {
            return res.json();
          } else {
            throw res;
          }
        })
        .then((data) => {
          setSuccess(true);
          setResponse(props.submitSuccess);

          if (props.submitSuccessCallback) {
            props.submitSuccessCallback(data);
          }
        })
        .catch((error) => {
          console.log(error);
          error.json().then((err) => {
            setError(true);
            setResponse(err);

            if (props.submitErrorCallback) {
              props.submitErrorCallback(err);
            }
          });
        });
    }
  };

  return (
    <React.Fragment>
      {response && (
        <StatusMessage error={error} success={success} content={response} />
      )}
      {(!props.fetch && !props.hideForm) || hasFetched ? (
        <React.Fragment>
          <form onSubmit={handleFormSubmit}>
            {props.fields &&
              props.fields.map((field, index) => {
                return (
                  <div key={index} className='input-group'>
                    {props.labels && (
                      <label htmlFor={field.name}>{field.niceName}:</label>
                    )}
                    <input
                      type={field.type}
                      name={field.name}
                      id={field.name}
                      onChange={handleFormChange}
                      value={fields[field.name]}
                      placeholder={field.niceName}
                      checked={
                        field.type == 'checkbox' && fields[field.name]
                          ? 'checked'
                          : undefined
                      }
                      required={field.required}
                      autoComplete={
                        field.autocomplete ? field.autocomplete : 'false'
                      }
                    />
                    <div className='input-description'>{field.description}</div>
                  </div>
                );
              })}

            <input type='submit' value='Submit' />
          </form>
        </React.Fragment>
      ) : (
        <React.Fragment>{props.fetch && <LoadingIndicator />}</React.Fragment>
      )}
    </React.Fragment>
  );
};

export default Form;
