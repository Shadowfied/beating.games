import React from 'react';
import { Link } from 'react-router-dom';
import Avatar from './UI/Avatar';

const UserBanner = (props) => {
  return (
    <React.Fragment>
      <div
        className={`user-banner ${props.sticky ? 'sticky' : ''}`}
        style={{
          backgroundImage: `url(${props.background})`,
          backgroundPosition: props.backgroundPosition,
          backgroundAttachment: props.backgroundFixed ? 'fixed' : '',
        }}
      >
        <div className='container'>
          <Avatar background={props.avatar} size='large' />
          <div className='user-profile-info' style={{ marginTop: '1rem' }}>
            <Link
              className='user-profile-name'
              style={{ display: 'block' }}
              to={`/user/${props.user}/`}
            >
              {props.user}'s profile
            </Link>
            <div className='user-profile-stats'>
              Currently playing {props.gameStats.inProgressGames} game
              {props.gameStats.inProgressGames !== 1 ? 's' : ''}
              &nbsp;- Continuously playing {props.gameStats.continualGames} game
              {props.gameStats.continualGames !== 1 ? 's' : ''}
              &nbsp;- Beaten {props.gameStats.beatenGames} game
              {props.gameStats.beatenGames !== 1 ? 's' : ''}
              &nbsp;- Want to play {props.gameStats.wantToPlayGames} game
              {props.gameStats.wantToPlayGames !== 1 ? 's' : ''}
              &nbsp;- Dropped {props.gameStats.droppedGames} game
              {props.gameStats.droppedGames !== 1 ? 's' : ''}
            </div>
          </div>
        </div>
      </div>
      <div className={`user-banner-padder ${props.sticky ? '' : 'hidden'}`} />
    </React.Fragment>
  );
};

export default UserBanner;
