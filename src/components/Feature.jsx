import React from 'react';

const Feature = (props) => {
	return (
		<div className="feature">
			<i className="material-icons">{props.icon}</i>
			<div className="feature__title">{props.title}</div>
			<div className="feature__content">
				{props.children}
			</div>
		</div>
	);
}

export default Feature;