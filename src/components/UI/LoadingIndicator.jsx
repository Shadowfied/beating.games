import React from 'react';

const LoadingIndicator = () => {
	return (
		<div className="loading-spinner">
			<div />
			<div />
			<div />
			<div />
		</div>
	);
}

export default LoadingIndicator;