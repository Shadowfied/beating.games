import React from 'react';

const StatusMessage = (props) => {
	let statusClasses = '';
	if (props.success) {
		return (
			<div className="status-message status-message--success">
				{props.content}
			</div>
		);
	}

	if (props.error) {
		return (
			<div className="status-message status-message--error">
				{props.content}
			</div>
		);
	}
};

export default StatusMessage;