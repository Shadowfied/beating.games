import React, { useState } from 'react';

const SpoilerTag = (props) => {
  const [isVisible, setIsVisible] = useState(false);

  return (
    <span
      className={`spoiler-${isVisible ? 'visible' : 'hidden'}`}
      onClick={() => setIsVisible(!isVisible)}
    >
      {isVisible
        ? props.content
        : props.content
            .split('')
            .map((char) =>
              char !== ' '
                ? String.fromCharCode(Math.random() * (80 - 60) + 80)
                : char
            )}
    </span>
  );
};

export default SpoilerTag;
