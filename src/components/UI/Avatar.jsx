import React from 'react';

const Avatar = (props) => {
	return (
		<div className={`avatar ${props.className ? props.className : ''} ${props.size ? 'avatar--' + props.size : ''}`} style={{backgroundImage: `url(${props.background ? props.background : 'https://i.imgur.com/ziluww5.png'}`}} />
	);
};

export default Avatar;