import React from 'react';
import { useAuth } from '../context/auth';

import reactStringReplace from 'react-string-replace';

import SpoilerTag from './UI/SpoilerTag';

const Game = (props) => {
  const { authData } = useAuth();
  const game = props.game;

  const deleteGame = () => {
    if (window.confirm('Are you sure you want to delete?')) {
      if (authData) {
        fetch(`https://api.beating.games/game/delete`, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: authData.token,
          },
          body: JSON.stringify({ gameId: game._id }),
        }).then(() => {
          props.update();
        });
      }
    }
  };

  const editGame = () => {
    props.edit(props.game);
  };

  const getSpoilerTags = (string) => {
    return reactStringReplace(
      string,
      /\|\|\n?([\s\S]*?)\n?\|\|/g,
      (match, i) => {
        return <SpoilerTag key={i} content={match} />;
      }
    );
  };

  const renderAchievementIcon = () => {
    if (!game.achievementsLink && !game.perfectGame) return;

    const Tag = game.achievementsLink ? 'a' : 'span';
    let classes = 'game-box__icon game-box__achievements';

    if (game.perfectGame)
      classes = classes + ' game-box__achievements--perfect';

    return (
      <Tag
        href={Tag === 'a' ? game.achievementsLink : undefined}
        target={Tag === 'a' ? '_blank' : undefined}
        className={classes}
      >
        <i className='material-icons'>emoji_events</i>
      </Tag>
    );
  };

  return (
    <div className='game-box'>
      <div className='game-box__image'>
        <img src={game.background} loading='lazy' />
      </div>
      <div className='game-box__content'>
        <div className='game-box__date'>{game.date}</div>
        <div className='game-box__system'>{game.system}</div>
        <div className='game-box__title'>{game.title}</div>
        <div className='game-box__playtime'>{game.playtime}</div>
        <div className='game-box__status'>{game.status}</div>
      </div>
      <div className='game-box__hover-wrapper'>
        {renderAchievementIcon()}
        <a
          href={game.background}
          target='_blank'
          className='game-box__icon game-box__open-background'
        >
          <i className='material-icons'>open_in_new</i>
        </a>
        {authData && props.user === authData.user && (
          <React.Fragment>
            <div className='game-box__icon game-box__edit' onClick={editGame}>
              <i className='material-icons'>edit</i>
            </div>
            <div
              className='game-box__icon game-box__delete'
              onClick={deleteGame}
            >
              <i className='material-icons'>delete</i>
            </div>
          </React.Fragment>
        )}
        <div className='game-box__hover-content'>
          <div className='game-box__hover-content-inner'>
            <div style={{ marginBottom: 'auto' }}>
              Added on:{' '}
              {new Date(
                parseInt(game._id.substring(0, 8), 16) * 1000
              ).toLocaleDateString()}
            </div>
            <br />
            {getSpoilerTags(game.content)}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Game;
