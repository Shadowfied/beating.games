import React, { useState, useEffect } from 'react';
import { useAuth } from '../context/auth';
import { useForm } from 'react-hook-form';

const NewGameForm = (props) => {
  const { handleSubmit, register, errors, reset } = useForm();

  const { authData } = useAuth();
  const [isInEditMode, setIsInEditMode] = useState(false);
  const defaultValues = {
    date: '',
    title: '',
    system: '',
    background: '',
    playtime: '',
    status: "What's the status?",
    owned: '',
    content: '',
  };

  const [values, setValues] = useState(defaultValues);
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    if (props.editGame) {
      reset(props.editGame);
      setIsInEditMode(true);
    }
  }, [props.editGame, setIsInEditMode]);

  const onSubmit = (formValues) => {
    if (authData) {
      let apiUrl = '';

      if (isInEditMode) {
        apiUrl = 'https://api.beating.games/game/update';
      } else {
        apiUrl = 'https://api.beating.games/game/insert';
      }

      fetch(apiUrl, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: authData.token,
        },
        body: JSON.stringify(
          isInEditMode ? { ...props.editGame, ...formValues } : formValues
        ),
      }).then(() => {
        props.update();
        setValues(defaultValues);
        reset(defaultValues);
        setIsInEditMode(false);
      });

      setIsOpen(false);
    }
  };

  const stopEditing = () => {
    setValues(defaultValues);
    reset(defaultValues);
    setIsOpen(false);
    setIsInEditMode(false);
  };

  if (authData && authData.user == props.user) {
    return (
      <React.Fragment>
        <div className='new-game-form-container'>
          {!isOpen && !isInEditMode && (
            <button onClick={() => setIsOpen(!isOpen)}>Add new game</button>
          )}
          {isInEditMode && (
            <React.Fragment>
              You are currently editing {values.title}
            </React.Fragment>
          )}
          {isOpen || isInEditMode ? (
            <form className='new-game-form' onSubmit={handleSubmit(onSubmit)}>
              <input
                type='text'
                name='title'
                placeholder='Title..'
                defaultValue={values.title ? values.title : ''}
                ref={register}
                required
              />

              <select
                name='status'
                defaultValue={
                  values.status ? values.status : "What's the status?"
                }
                ref={register}
                required
              >
                <option value="What's the status?" disabled>
                  What's the status?
                </option>
                <option value='Beaten'>Beaten</option>
                <option value='Continual'>Continual</option>
                <option value='In Progress'>In Progress</option>
                <option value='Want To Play'>Want To Play</option>
                <option value='Dropped'>Dropped</option>
              </select>

              <input
                type='text'
                name='system'
                placeholder='System..'
                defaultValue={values.system ? values.system : ''}
                ref={register}
                required
              />

              <input
                type='text'
                name='playtime'
                placeholder='Playtime..'
                defaultValue={values.playtime ? values.playtime : ''}
                ref={register}
              />

              <input
                type='url'
                name='background'
                placeholder='Background..'
                defaultValue={values.background ? values.background : ''}
                ref={register}
              />

              <input
                type='date'
                name='date'
                defaultValue={values.date}
                ref={register}
              />

              <textarea
                name='content'
                placeholder='Content'
                defaultValue={values.content ? values.content : ''}
                ref={register}
              />

              <input
                type='checkbox'
                id='owned'
                name='owned'
                defaultChecked={values.owned}
                defaultValue={values.owned}
                ref={register}
              />
              <label htmlFor='owned'>Do you own the game?</label>

              <input
                type='text'
                name='achievementsLink'
                placeholder='Link to achievements for the game'
                defaultValue={
                  values.achievementsLink ? values.achievementsLink : ''
                }
                ref={register}
              />

              <input
                type='checkbox'
                id='perfectGame'
                name='perfectGame'
                defaultChecked={values.perfectGame}
                defaultValue={values.perfectGame}
                ref={register}
              />
              <label htmlFor='perfectGame' style={{ display: 'inline' }}>
                Did you perfect (all achievements / platinum) the game?
              </label>

              <input
                className='button--primary'
                type='submit'
                value={isInEditMode ? 'Update game' : 'Add game'}
              />
            </form>
          ) : (
            <React.Fragment />
          )}
          {isInEditMode && <button onClick={stopEditing}>Stop editing</button>}
          {isOpen && !isInEditMode && (
            <button onClick={() => setIsOpen(!isOpen)}>Discard</button>
          )}
        </div>
      </React.Fragment>
    );
  } else {
    return <React.Fragment />;
  }
};

export default NewGameForm;
