import React, { useState } from 'react';
import { useAuth } from '../context/auth';
import Form from './Form';

const LoginForm = (props) => {
	const { authData, setAuthData } = useAuth();

	const onLogin = (data) => {
		setAuthData(data);
	};

	return (
		<div className="login-form">
			<Form 
				submit="https://api.beating.games/login"
				submitSuccessCallback={onLogin}
				submitError="Invalid username or password"
				fields={[
					{
						type: "text",
						name: "username",
						niceName: "Username"
					},
					{
						type: "password",
						name: "password",
						niceName: "Password"
					}
				]}
			/>
		</div>
	);
}

export default LoginForm;